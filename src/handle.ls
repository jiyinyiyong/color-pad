
hue = 0

get = (id) -> document.getElementById id

p2 = -> pow it, 2

d = (x) -> abs (x - 100)

for key, val of prelude then window[key] = val
show = (...args) -> console.log.apply console, args

cal-h = (x, y) ->
    y = -y
    h = (atan2 x, y) * 180 / pi
    if h < 0 then h += 360
    h

cal-color = (x, y) ->
  # c = 100 # set center at 100, 100
  distance = sqrt sum map p2, [x, y]
  if distance > 100
    'hsl(0,100%,100%)'
  else
    h = cal-h x, y
    s = 100
    l = 60
    "hsl(#h,#s%,#l%)"

dep-color = (x, y) ->
  s = round (x / 5)
  l = round (y / 5)
  ret = "hsl(#hue,#s%,#l%)"
  show ret
  ret

window.onload = ->
  box = get \box
  crl = get \circle

  b = box.getContext \2d
  window.c = crl.getContext \2d

  show \kk

  for i in [-100 til 100]
    for j in [-100 til 100]
      c.fillStyle = cal-color i, j
      c.fillRect (100 + i), (100 + j), 1, 1

  render = ->
    b.clearRect 0, 0, 500, 500
    for j in [0 til 500]
      l = j / 5
      gra = b.createLinearGradient 0, 0, 500, 0
      gra.addColorStop 0, "hsl(#hue,0%,#l%)"
      gra.addColorStop 1, "hsl(#hue,100%,#l%)"
      b.fillStyle = gra # dep-color i, j
      b.fillRect 0, j, 500, 1

    box.onclick = (event) ->
      x = event.pageX - box.offsetLeft
      y = event.pageY - box.offsetTop
      show x, y
      color = dep-color x, y
      get(\sample).style.background = color
      get('hsl').value = color

  crl.onclick = (event) ->
    x = event.pageX - crl.offsetLeft - 100
    y = event.pageY - crl.offsetTop - 100
    window.hue = round cal-h x, y
    do render

  get('hsl').onmouseover = ->
    @select()

  get('hsl').oninput = ->
    color = @value
    get(\sample).style.background = color