
cd `dirname $0`

jade -O ./ -wP src/*jade &
stylus -o ./ -w src/*styl &
livescript -o ./ -wbc src/*ls &
doodle *html *js *css &

read

pkill -f jade
pkill -f stylus
pkill -f livescript
pkill -f doodle